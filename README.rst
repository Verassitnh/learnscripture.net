README
======

This is the source code and `issue tracker <https://gitlab.com/learnscripture/learnscripture.net/issues>`_ for `LearnScripture.net <https://learnscripture.net/>`_

Documentation is found in the `docs/ <https://gitlab.com/learnscripture/learnscripture.net/tree/master/docs>`_ folder.
