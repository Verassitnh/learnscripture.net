# -*- coding: utf-8 -*-
# Generated by Django 1.11.4 on 2017-09-05 14:03
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('accounts', '0008_remove_account_has_installed_android_app'),
        ('bibleverses', '0008_textversion_language_code'),
    ]

    operations = [
        migrations.RenameField(
            model_name='qapair',
            old_name='reference',
            new_name='localized_reference',
        ),
        migrations.RenameField(
            model_name='userversestatus',
            old_name='reference',
            new_name='localized_reference',
        ),
        migrations.RenameField(
            model_name='verse',
            old_name='reference',
            new_name='localized_reference',
        ),
        migrations.RenameField(
            model_name='versechoice',
            old_name='reference',
            new_name='localized_reference',
        ),
        migrations.RenameField(
            model_name='wordsuggestiondata',
            old_name='reference',
            new_name='localized_reference',
        ),
        migrations.AlterUniqueTogether(
            name='qapair',
            unique_together=set([('catechism', 'order'), ('catechism', 'localized_reference')]),
        ),
        migrations.AlterUniqueTogether(
            name='userversestatus',
            unique_together=set([('for_identity', 'verse_set', 'localized_reference', 'version')]),
        ),
        migrations.AlterUniqueTogether(
            name='verse',
            unique_together=set([('localized_reference', 'version'), ('bible_verse_number', 'version')]),
        ),
        migrations.AlterUniqueTogether(
            name='versechoice',
            unique_together=set([('verse_set', 'localized_reference')]),
        ),
        migrations.AlterUniqueTogether(
            name='wordsuggestiondata',
            unique_together=set([('version_slug', 'localized_reference')]),
        ),
    ]
