LearnScripture.net is web site created using the Python programming language,
the `Django <https://www.djangoproject.com/>`_ web framework, and various other technologies.

If you are familiar with these or are willing to learn, we'd love to have your help
in maintaining and improving LearnScripture.net.

Docs you will need to read:

* Get set up:

  - development_setup.rst

* Understand the project

  - architecture.rst
  - project_structure.rst
  - development_principles.rst
  - development_environment.rst
  - organisation_structure.rst

* Detailed coding issues

  - coding_standards.rst
  - code_notes.rst

* Understand deployment

  - deployment.rst
  - server_setup.rst
  - DNS_setups.rst

* Other things

  - android_app.rst
  - todo.rst
